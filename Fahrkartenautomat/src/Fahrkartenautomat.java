﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static void main(String[] args) {
				
		do { 
			double zuZahlenderBetrag = fahrkartenBestellungErfassen();
			double eingezahlt = fahrkartenBezahlen(zuZahlenderBetrag);
	
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlt, zuZahlenderBetrag);
			
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt. ");
		
		} while (true);
		
	}

	public static double fahrkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int anfrageTickets = 0;
		int ihreWahlTickets = 0;
		boolean falscheAuswahl = true;
		boolean bezahlen = true;
		
		double Einzelfahrschein = 2.90;
		double Tageskarte = 8.60;
		double Kleingruppen = 23.50;
	
		boolean anzahlStimmtNicht = true;

		while (bezahlen) {
			falscheAuswahl = true;
			System.out.println("Wählen Sie aus:\n");
			System.out.println("- Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("- Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("- Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("- Möchten Sie bezahlen? (9)");

			while (falscheAuswahl) {

				ihreWahlTickets = tastatur.nextInt();

				if (ihreWahlTickets == 3 || ihreWahlTickets == 2 || ihreWahlTickets == 1 || ihreWahlTickets == 9) {
					falscheAuswahl = false;
				} else {
					System.out.print("Falsche eingabe, bitte wählen Sie 1, 2, 3 oder 9!");
				}
			}

			switch (ihreWahlTickets) {
			case 1:
				System.out.println("Ihre Wahl: 1");
				zuZahlenderBetrag = Einzelfahrschein;
				anzahlStimmtNicht = true;
				break;
			case 2:
				System.out.println("Ihre Wahl: 2");
				zuZahlenderBetrag = Tageskarte;
				anzahlStimmtNicht = true;
				break;
			case 3:
				System.out.println("Ihre Wahl: 3");
				zuZahlenderBetrag = Kleingruppen;
				anzahlStimmtNicht = true;
				break;
			case 9:
				System.out.println("Ihre Wahl: 9");
				anzahlStimmtNicht = false;
				bezahlen = false;
				zuZahlenderBetrag = zuZahlenderBetrag;
				break;
			default:
				System.out.println("Sie haben eine falsche Fahrkarte gewählt!");
				zuZahlenderBetrag = 0;
				anzahlStimmtNicht = true;
				break;
			}

			while (anzahlStimmtNicht) {

				System.out.print("Anzahl Tickets eingeben 1 bis 10 Tikets:");
				anfrageTickets = tastatur.nextInt();

				if (anfrageTickets >= 11) {
					anzahlStimmtNicht = true;
					System.out.println("Anzahl Tickets ist größer 10!");

				} else if (anfrageTickets <= 0) {
					anzahlStimmtNicht = true;
					System.out.println("Anzahl Tickets ist kleiner 1!");

				} else {
					anzahlStimmtNicht = false;
					System.out.println("Anzahl Tickets passt!");
				}
			}

			zuZahlenderBetrag = zuZahlenderBetrag * anfrageTickets;
			System.out.println("\n Momentan zu Zahlender Preis:" + zuZahlenderBetrag + "0 Euro \n \n" );

		}
		return zuZahlenderBetrag;
	}


	public static double fahrkartenBezahlen(double zuZahlen) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		// Geldeinwurf
		// ----------- //
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			double ausstehend = (zuZahlen - eingezahlterGesamtbetrag);
			System.out.printf("Noch zu zahlen: %.2f Euro \n", ausstehend);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
		
	}
	
	
	public static void rueckgeldAusgeben(double eingezahlt, double zuZahlenderBetrag) {
		
		double rückgabebetrag;

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rückgabebetrag = eingezahlt - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO \n");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.04)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}
		
	}
	
	
	
	public static void warte(int milisec) {
		try {
			Thread.sleep(milisec);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag+" "+einheit);
	}
	
}