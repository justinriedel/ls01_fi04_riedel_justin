package konsolenausgabe;

public class Fakultaet {

	public static void main(String[] args) {
	
		Integer calc_to = 5;
		
		Fakultaet fakultaet = new Fakultaet();
	
		for (int i = 0; i <= calc_to; ++i) {	
			String finfo = i+"!";
			String result = fakultaet.berechne(i)+"";			
			String calc = "";
			for (int j = 1; j <= i; ++j) {
				calc+=j;
				if(j!=i) {
					calc+=" * ";
				}
			}
			System.out.printf("%-5s= %-17s = %-4s\n", finfo, calc, result);			
		}
	}
	
	
	public int berechne(int zahl)  {
		int fakultaet = 1; 
		for (int i = 1; i <= zahl; ++i) {
			fakultaet = fakultaet * i;
		}
		return fakultaet;
	}

}
