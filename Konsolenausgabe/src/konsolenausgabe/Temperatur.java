package konsolenausgabe;

import java.text.DecimalFormat;

public class Temperatur {

	private static DecimalFormat DecimalFormat = new DecimalFormat("0.00");

	public static void main(String[] args) {
		
		/* L�SUNG 1
		 * 
		 *  Statische Ausgabe
		 * 
		 */
		System.out.println("L�SUNG 1:\n");
		
		
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");			
		System.out.printf("-----------------------\n");			
		System.out.printf("%+-12d|%4s%.2f\n", -20, "", -28.8889);			
		System.out.printf("%+-12d|%4s%.2f\n", -10, "", -23.3333);			
		System.out.printf("%+-12d|%4s%.2f\n", 0, "", -17.7778);			
		System.out.printf("%+-12d|%5s%.2f\n", 20, "", -6.6667);			
		System.out.printf("%+-12d|%5s%.2f\n", 30, "", -1.1111);			
	
		
		/* L�SUNG 2
		 * 
		 *  Kann jederzeit erweitert werden, indem neue Eintr�ge zum Object hinzuf�gt werden.
		 * 
		 */
		System.out.println("\n\nL�SUNG 2:\n");
			
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");			
		System.out.printf("-----------------------\n");			
		
		Object[][] temperaturen = {{-20, -28.8889},
								   {-10, -23.3333},		
								   {0, -17.7778},
								   {20,-6.6667},
								   {30,-1.1111}};
			  
		  for (int i = 0; i < temperaturen.length; ++i) {  
			  String celsius_output = DecimalFormat.format(temperaturen[i][1]);
			  System.out.printf("%+-12d|%10s\n", temperaturen[i][0], celsius_output);					        
		  }
	}
	
}
